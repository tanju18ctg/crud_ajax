<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //read data

    public function index(){
        $data = Employee::latest()->get();
        return response()->json($data);
    }

    //store data

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'position' => 'required',
            'employee_id' => 'required',
            'joining_date' => 'required'
        ]);

        $data = Employee::insert([
            'name' => $request->name,
            'position' => $request->position,
            'phone' => $request->phone,
            'email' => $request->email,
            'employee_id' => $request->employee_id,
            'joining_date' => $request->joining_date,
        ]);

        return response()->json($data);

    }
}
