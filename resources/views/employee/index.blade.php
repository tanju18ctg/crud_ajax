<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/fontawesome.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <style>
        form {
            width: 100%;
        }
    </style>
</head>

<body>

    <div class="body-content">
        <div class="container">
            <div class="row">
                {{-- <form action="#" method="post"> --}}
                    @csrf
                    <div class="col-md-12">
                        <div class="row">
                            <!-- already-registered-login -->
                            <div class="col-md-12">
                                <h2 class="text-success text-center mb-3"> Employee Registered Form </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1"> Name
                                        <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input"
                                        placeholder="name" name="Name" id="name">
                                        <span class="text-danger" id="nameError"></span>
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputPassword1">Email
                                        <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input"
                                        placeholder="email" name="email" id="email" value="">
                                        <span class="text-danger" id="emailError"></span>
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputPassword1">Phone
                                        <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input"
                                        placeholder="Phone" name="phone" id="phone" value="">
                                        <span class="text-danger" id="phoneError"></span>
                                </div>

                            </div>
                            <!-- already-registered-login -->
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputPassword1">Position
                                        <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input"
                                        placeholder="position" name="position" id="position">
                                        <span class="text-danger" id="positionError"></span>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputPassword1">Employee Id
                                        <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input"
                                        placeholder="employee_id" name="employee_id" id="employee_id" value="">
                                        <span class="text-danger" id="employeeError"></span>
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputPassword1">Joining Date
                                        <span>*</span></label>
                                    <input type="date" class="form-control unicase-form-control text-input"
                                        name="joining_date" value="" id="joining_date">
                                        <span class="text-danger" id="joiningDateError"></span>
                                </div>


                            </div>

                            <div class="m-auto">
                                <button class="btn btn-primary" onclick="storeData()">Add Employee</button>
                            </div>
                        </div>
                    </div>

                {{-- </form> --}}
            </div><!-- /.row -->

            <div class="row row-sm mt-5">
                <!--------list-page------->
                <div class="col-md-12">
                    <div class="card pd-20 pd-sm-40">
                        <h4 class="card-body-title text-center">Employee List</h4>
                        <div class="table-wrapper">
                            <table id="datatable1" class="table display responsive nowrap">
                                <thead>
                                    <tr>
                                        <th class="wd-15p">Employee Name </th>
                                        <th class="wd-15p">Email</th>
                                        <th class="wd-15p">Phone</th>
                                        <th class="wd-15p">Position</th>
                                        <th class="wd-15p">Employee Id</th>
                                        <th class="wd-15p">Joining Date</th>
                                        <th class="wd-15p">Action</th>
                                    </tr>
                                </thead>
                                <tbody id=table-body>
                                    {{-- <tr>
                                        <td>sd fsd fsd</td>
                                        <td> gdg gfd g</td>
                                        <td>tewtwetew৳</td>
                                        <td>tewtwetew৳</td>
                                        <td>tewtwetew৳</td>
                                        <td>65</td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-sm"> <i class="fa fa-eye">
                                                </i>View</a>
                                            <a href="#" class="btn btn-primary btn-sm"> <i class="fa fa-pencil">
                                                </i>Edit</a>
                                            <a href="#" class="btn btn-danger btn-sm" id="delete"><i
                                                    class="fa fa-trash"> </i> Delete </a>
                                        </td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div><!-- table-wrapper -->
                    </div><!-- card -->
                </div>


            </div>
        </div>
    </div>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });


        //read data methods

        function getData(){
            $.ajax({
                method : 'GET',
                url: '/all-employee',
                dataType: 'json',
                success: function (res) {
                    var data = "";
                    $.each(res, function (key, value) {
                     data = data + "<tr>"
                     data = data + "<td>"+value.id+"</td>"
                     data = data + "<td>"+value.name+"</td>"
                     data = data + "<td>"+value.position+"</td>"
                     data = data + "<td>"+value.email+"</td>"
                     data = data + "<td>"+value.phone+"</td>"
                     data = data + "<td>"+value.employee_id+"</td>"
                     data = data + "<td>"+value.joining_date+"</td>"
                     data = data + "<td>"
                     data = data +  "<button class='btn btn-success btn-sm'> View </button>"
                     data = data +  "<button class='btn btn-primary btn-sm'> Edit </button>"
                     data = data +  "<button class='btn btn-danger btn-sm'> Delete </button>"
                     data = data + "</td>"
                     data = data + "</tr>"
                    })
                    $('#table-body').html(data);
                }
            })
        }

        getData();


        // store data function

        function storeData(){
            var name = $("#name").val();
            var position = $("#position").val();
            var phone = $("#phone").val();
            var email = $("#email").val();
            var employee_id = $("#employee_id").val();
            var joining_date = $("#joining_date").val();

            $.ajax({
                method: "POST",
                url : "/add-employee",
                dataType : 'JSON',
                data : {name : name, position:position, phone:phone, email:email, employee_id:employee_id, joining_date:joining_date},
                success : function (res) {
                    getData();
                    console.log('data store success');
                },
                error : function (error) {
                    console.log(error.responseJSON.errors);
                    $("#nameError").text(error.responseJSON.errors.name);
                    $("#phoneError").text(error.responseJSON.errors.phone);
                    $("#positionError").text(error.responseJSON.errors.position);
                    $("#emailError").text(error.responseJSON.errors.email);
                    $("#employeeError").text(error.responseJSON.errors.employee_id);
                    $("#joiningDateError").text(error.responseJSON.errors.joining_date);
                }
            })
        }
    </script>


</body>

</html>
